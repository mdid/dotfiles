if empty(glob('~/.config/nvim/autoload/plug.vim'))
"	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p $HOME/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > $HOME/.config/nvim/autoload/plug.vim
"	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "$HOME/.config/nvim/autoload/plugged"'))
" FUNCTIONALITY
Plug 'tpope/vim-surround' " changing surrounding like ' to something else. Command is cs'<q> it will change ' to <q>
Plug 'junegunn/goyo.vim' " distraction free writing in VIM. command is :Goyo
" Plug 'vimwiki/vimwiki' " wiki usage
Plug 'tpope/vim-commentary' " comment stuff out
" Plug 'preservim/nerdtree' " file tree
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' } " visualising colros in vim
" Plug 'jreybert/vimagit' " git commands in vim


" LOOK AND FEEL
Plug 'romgrk/barbar.nvim' " tabs on top of the screen
Plug 'kyazdani42/nvim-web-devicons' " icons for tabs
Plug 'arcticicestudio/nord-vim' " color nord
Plug 'altercation/vim-colors-solarized' " color solarized
Plug 'morhetz/gruvbox' " color gruvbod
Plug 'chriskempson/tomorrow-theme'
Plug 'chriskempson/base16-vim'
Plug 'jeffkreeftmeijer/vim-dim'
" Plug 'bling/vim-airline' " line on the bottom of the vim
" Plug 'itchyny/lightline.vim' " line on the bottom of the vim
call plug#end()
