GPG_TTY=$(tty)
export GPG_TTY

bindkey -v
_fix_cursor() {
   echo -ne '\e[5 q'
}

precmd_functions+=(_fix_cursor)

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt EXTENDED_HISTORY
# End of lines configured by zsh-newuser-install

# AUTOCOMPLETE
autoload -Uz compinit bashcompinit
compinit
bashcompinit
## autocompletion using arrow keys (based on history)
bindkey '<Up>' history-search-backward
bindkey '<Down>' history-search-forward

#
# HISTORY AUTOCOMPLETE

# history search

#autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
#zle -N up-line-or-beginning-search
#zle -N down-line-or-beginning-search
#autoload -U history-search-end
#[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
#[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# alias

alias vim='nvim'
alias vi='nvim'
alias ls='eza -al --color=always --group-directories-first' #my preferred listing
alias la='eza -a --color=always --group-directories-first'  # all files and dirs
alias ll='eza -l --color=always --group-directories-first'  # long format
alias lt='eza -aT --color=always --group-directories-first' # tree listing
alias ld='eza -s=mod -l --color=always --group-directories-first --icons' # tree listing


alias mirror="sudo reflector -f 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias df='df -h'
alias psmem='ps auxf | sort -nr -k 4'
alias soundcore='bluetoothctl power on && sleep 1s && bluetoothctl connect 98:52:3D:8A:B0:F8 && echo "end"'
alias music='mpv --no-video --term-title='${chapter}' --term-osd-bar --term-playing-msg='${media-title}' --fs'
# alias bitwarden="bitwarden-desktop --ozone-platform-hint=auto --enable-wayland-ime"

source /home/md/.config/broot/launcher/bash/br

# Prompts
autoload -Uz promptinit
autoload -Uz vcs_info
precmd() { vcs_info }
promptinit
zstyle ':vcs_info:git:*' formats '%b '
setopt PROMPT_SUBST
PROMPT='%F{green}%*%f %F{blue}%~%f %F{red}${vcs_info_msg_0_}%f$ '



source /usr/share/zsh/plugins/fzf-tab-git/fzf-tab.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh

ZSH_AUTOSUGGEST_STRATEGY=(history completion)

# Created by `pipx` on 2023-11-21 23:22:35
export PATH="$PATH:/home/md/.local/bin"




export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
