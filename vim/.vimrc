"======================
"=== INITIAL CONFIG ===
"======================

let mapleader =","
set nocompatible
syntax enable
set laststatus=2
set number
set mouse=a
set nohlsearch
set clipboard+=unnamedplus
" set noruler
set noshowcmd


"======================
"=== FILE  EXPLORER ===
"======================

set path+=**
set path+=/home/md/**
filetype plugin on
set wildmenu


let g:netrw_browse_split=4
let g:netrw_winsize=25
let g:netrw_altv=1
let g:netrw_liststyle=1
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s)zs\.\S\+'

" to activate it use commands
" :Explore - opens netrw in the current window
" :Sexplore - opens netrw in a horizontal split
" :Vexplore - opens netrw in a vertical split

" Source
" https://shapeshed.com/vim-netrw/


"======================
"=== CURSOR   MODES ===
"======================
let &t_SI = "\<Esc>]50;CursorShape=1\x7" " SI = Inster mode
let &t_SR = "\<Esc>]50;CursorShape=2\x7" " SR = Replace mode
let &t_EI = "\<Esc>]50;CursorShape=0\x7" " EI = Normal mode


"======================
"===     WRITING    ===
"======================

" Enable autocompletion:
	set wildmode=longest,list,full

" Spell-check set to <leader>o, 'o' for 'orthography':
	map <leader>o :setlocal spell! spelllang=en_us<CR>
" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
	set splitbelow splitright

" Replace ex mode with gq
	map Q gq

" Check file in shellcheck:
	map <leader>s :!clear && shellcheck -x %<CR>


" Open corresponding .pdf/.html or preview
	map <leader>p :!opout <c-r>%<CR><CR>


" Ensure files are read as what I want:
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
autocmd BufRead,BufNewFile *.tex set filetype=tex
autocmd BufRead,BufNewFile *.md set filetype=markdown

" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Enable Goyo by default for mutt writing
	autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
	autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo | set bg=light
	autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
	autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

" Automatically deletes all trailing whitespace and newlines at end of file on save.
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritePre * %s/\n\+\%$//e
	autocmd BufWritePre *.[ch] %s/\%$/\r/e

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
if &diff
    highlight! link DiffText MatchParen
endif





"======================
"===   TEMPLATES    ===
"======================

nnoremap ,itpost :-1read $HOME/.vim/templates/itpost.html<CR>





"======================
"===     PLUGINS    ===
"======================

if ! filereadable(system('echo -n "${DG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
" FUNCTIONALITY
Plug 'tpope/vim-surround' " changing surrounding like ' to something else. Command is cs'<q> it will change ' to <q>
Plug 'junegunn/goyo.vim' " distraction free writing in VIM. command is :Goyo
" Plug 'vimwiki/vimwiki' " wiki usage
Plug 'tpope/vim-commentary' " comment stuff out
" Plug 'preservim/nerdtree' " file tree
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' } " visualising colros in vim
" Plug 'jreybert/vimagit' " git commands in vim


" LOOK AND FEEL
Plug 'romgrk/barbar.nvim' " tabs on top of the screen
Plug 'kyazdani42/nvim-web-devicons' " icons for tabs
Plug 'arcticicestudio/nord-vim' " color nord
Plug 'altercation/vim-colors-solarized' " color solarized
Plug 'morhetz/gruvbox' " color gruvbod
" Plug 'bling/vim-airline' " line on the bottom of the vim
" Plug 'itchyny/lightline.vim' " line on the bottom of the vim
call plug#end()


" cmdlt sheet
" :b - buffer
" :bn - buffer next
" :brow ol - open list of recently edited files
"
" :find <file name> - search files
" :edit . - file manager
"
" ^E - scroll the window down
" ^Y - scroll the window up
" ^F - scroll down one page
" ^B - scroll up one page
" H - move cursor to the top of the window
" M - move cursor to the middle of the window
" L - move cursor to the bottom of the window
" gg - go to the top of the file
" G - go to the bottom of the file
" w - move to the begining for new word
" e - move to the end of the word
"
" d - delete (also cut)
" c - change (delet then place in insert mode)
" y - yank (copy)
" v - visual select
"
" p - paste below
" P - paste above
"
" dd - remove line
" yy - copy whole line
" D - remove from here
" I - go to the begining and change to Insert mode
" A - go to the end and change to Insert mode
"

"
"
"
colorscheme nord
let g:Hexokinase_highlighters = ['virtual']

nnoremap <silent>    <A-,> :BufferPrevious<CR>
nnoremap <silent>    <A-.> :BufferClose<CR>
