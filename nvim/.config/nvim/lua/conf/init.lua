
-- configuraiton
vim.o.relativenumber = true
vim.opt.termguicolors = true

-- plugins
require("conf.remap")
require("conf.lazy")
require("bufferline").setup{}
