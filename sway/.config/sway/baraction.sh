#!/bin/bash
# baraction.sh for spectrwm status bar

## DISK

  hdd="$(df -h | awk 'NR==4{print $3, $5}')"


## RAM
mem=`free | grep Mem | awk '{printf "%.0f", $3/$2 * 100.0}'`
# mem=`free | awk '/Mem/ {printf "%dM/%dM\n", $3 / 1024.0, $2 / 1024.0 }'`
## CPU
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))

## VOLUME
    vol=`pamixer --get-volume-human`
    mute="pamixer --get-mute"
    [ "$mute" = true ] && echo -e "婢" || echo -e "VOL:$vol"



## BATTERY

 #   bat=`cat /sys/class/power_supply/BAT0/capacity`



## NETWORK

ifname="$(ip a | awk '/lo|v/ {next} /state UP/ {print$2}')"
ip="$(ip a show $ifname | grep -Po 'inet \K[\d.]+')"
[ -z "$ip" ] && echo "DISCONNECTED" || echo "$ip"

wifi="$(ip a | grep wlp7s0 | grep inet | wc -l)"
myssid="$(iwgetid -r)"
[[ $wifi = 1 ]] && echo "直" || echo "睊"

eno1="$(ip a | grep eno1 | grep inet | wc -l)"
enp6s0="$(ip a | grep enp6s0 | grep inet | wc -l)"
[[ $eno1 = 1 || $enp6s0 = 1 ]] && echo "Online" || echo "Offline"

## DATE & TIME
  date_time="$(date '+%a %d %b %H:%M')"

echo CPU: $cpu RAM: $mem $network $vol $date_time
