#!/bin/bash
# baraction.sh for spectrwm status bar

## DISK
hdd() {
  hdd="$(df -h | awk 'NR==4{print $3, $5}')"
  echo -e "HDD: $hdd"
}

## RAM
mem() {
mem=`free | grep Mem | awk '{printf "%.0f", $3/$2 * 100.0}'`
# mem=`free | awk '/Mem/ {printf "%dM/%dM\n", $3 / 1024.0, $2 / 1024.0 }'`
  echo -e "RAM $mem%"
}

## CPU
cpu() {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo -e "CPU $cpu%"
}

## VOLUME
vol() {
    vol=`pamixer --get-volume-human`
    mute="pamixer --get-mute"
    [ "$mute" = true ] && echo -e "婢" || echo -e "VOL:$vol"

}

## BATTERY
bat() {
    bat=`cat /sys/class/power_supply/BAT0/capacity`
    echo -e "BAT $bat%"
}


## NETWORK

network(){
ifname="$(ip a | awk '/lo|v/ {next} /state UP/ {print$2}')"
ip="$(ip a show $ifname | grep -Po 'inet \K[\d.]+')"
[ -z "$ip" ] && echo "DISCONNECTED" || echo "$ip"
}

networkw() {
wifi="$(ip a | grep wlp7s0 | grep inet | wc -l)"
myssid="$(iwgetid -r)"
[[ $wifi = 1 ]] && echo "直" || echo "睊"
}

networke() {
eno1="$(ip a | grep eno1 | grep inet | wc -l)"
enp6s0="$(ip a | grep enp6s0 | grep inet | wc -l)"
[[ $eno1 = 1 || $enp6s0 = 1 ]] && echo "Online" || echo "Offline"
}

## DATE & TIME
date_time() {
  date_time="$(date '+%a %d %b %H:%M')"
  echo -e "$date_time"
}

SLEEP_SEC=0.25
#loops forever outputting a line every SLEEP_SEC secs

# It seems that we are limited to how many characters can be displayed via
# the baraction script output. And the the markup tags count in that limit.
# So I would love to add more functions to this script but it makes the
# echo output too long to display correctly.
while :; do
	echo "+@fg=3;+@bg=3; $(cpu) +@fg=3;+@bg=4; $(mem) +@fg=3;+@bg=5; $(network) +@fg=3;+@bg=6; $(vol) +@fg=3;+@bg=7; $(date_time) +@fg=3;"
#    echo "+@fg=3;+@bg=3; $(cpu) +@fg=3;+@bg=4; $(mem) +@fg=3;+@bg=5; $(vol) +@fg=3;+@bg=6; $(networkw) +@fg=3;+@bg=6; $(networke) +@fg=3;+@bg=7; $(date_time) +@fg=3;"
#    echo "+@fg=1;+@fg=3;+@bg=1; $(cpu) +@fg=2;+@fg=3;+@bg=2; $(mem) +@fg=1;+@fg=3;+@bg=1; $(vol) +@fg=2;+@fg=3;+@bg=2; $(network) +@fg=1;+@fg=3;+@bg=1; $(date_time) +@fg=3;+@bg=1;"
    sleep $SLEEP_SEC
done
